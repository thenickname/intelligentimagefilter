from featureextraction.DescriptionsProvider import DescriptionsProvider
from featureextraction.VocabularyOptimizer import VocabularyOptimizer
from imagesearch.GoogleImageProvider import GoogleImageProvider
from preprocessing.Preprocessor import Preprocessor

def generateDescriptions( imageSet, baseDir, imgCount, siftEdge = 30, siftPeak = 3 ) :
  images = GoogleImageProvider( imageSet, baseDir, imgCount )
  preprocSteps = { 
    "resize" : 500,
    "colorSpace" : "L"
  }
  preprocImgs = Preprocessor( images, preprocSteps, "tempcache/preproc/" )
  return DescriptionsProvider( preprocImgs, siftEdge, siftPeak, "tempcache/sift/" )

optimizer = VocabularyOptimizer( generateDescriptions( "train", "../img/custom/cherry/", 40 ),
                                 generateDescriptions( "train", "../img/custom/__noise__/", 190 ),
                                 generateDescriptions( "test", "../img/custom/cherry/", 10 ),
                                 generateDescriptions( "test", "../img/custom/__noise__/", 10 ),
                                 visWordsRange = ( 100, 1000 ),
                                 visWordsStep = 100,
                                 seedAttempts = 10 )

bestNumOfWords, numOfWordsErrorRate = optimizer.findBestNumOfWords()
bestSeed, seedErrorRate = optimizer.findBestSeedForNumOfWords( bestNumOfWords )

print "best K:", bestNumOfWords
print "best seed:", bestSeed