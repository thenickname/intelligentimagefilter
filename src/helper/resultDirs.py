def generateDirName( optionName, optionValue ) :
  result = optionName
  for key, value in optionValue.iteritems() :
    if key == "seed" and value != "best" :
      value = "predefined"
    result += "-" + key + "-" + str( value )
  return result

def createDirPaths( config, basePath ) :
  resultDirPath = basePath
  singleDirPaths = {}
  for key, value in config.iteritems() :
    if key == "imageSet" :
      resultDirPath += value.get( "searchTerm" ) + "/"
    else :
      resultDirPath += generateDirName( key, value ) + "/"
    singleDirPaths[ key ] = resultDirPath
  return resultDirPath, singleDirPaths