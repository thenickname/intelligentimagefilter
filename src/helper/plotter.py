'''
Created on 18.11.2012

@author: Manuel Beuttler
'''

import pylab as pl
import numpy as np
import Image

def plot_gallery_imlist(imagelist, titles, h, w, n_row=4, n_col=5,subtitle=None):
    """Helper function to plot a gallery of portraits"""
    fig = pl.figure(figsize=(1.8 * n_col, 2.4 * n_row))
    if subtitle:
        fig.suptitle(subtitle, fontsize=14, fontweight='bold')
    pl.subplots_adjust(bottom=0, left=.01, right=.99, top=.90, hspace=.35)
    for i in range(n_row * n_col):
        if i >= len(imagelist):
            break
        
        pl.subplot(n_row, n_col, i + 1)
        img= np.array(Image.open(imagelist[i]))
        pl.imshow(img)
        pl.title(titles[i], size=12)
        pl.xticks(())
        pl.yticks(())
        
    pl.show()

def plot_gallery_imlist_feats(imagelist, descriptions, titles, h, w, n_row=4, n_col=5,subtitle=None):
    """Helper function to plot a gallery of portraits"""
    fig = pl.figure(figsize=(1.8 * n_col, 2.4 * n_row))
    if subtitle:
        fig.suptitle(subtitle, fontsize=14, fontweight='bold')
    pl.subplots_adjust(bottom=0, left=.01, right=.99, top=.90, hspace=.35)
    for i in range(n_row * n_col):
        if i >= len(imagelist):
            break
        
        pl.subplot(n_row, n_col, i + 1)
        img= np.array(Image.open(imagelist[i]))
        pl.imshow(img)
        l1 = descriptions[ i ].featureLocations
        pl.plot(l1[:,0],l1[:,1],'ob')
        pl.title(titles[i], size=12)
        pl.xticks(())
        pl.yticks(())
        
    pl.show()

def plot_gallery(images, titles, h, w, n_row=3, n_col=4,subtitle=None):
    """Helper function to plot a gallery of portraits"""
    fig = pl.figure(figsize=(1.8 * n_col, 2.4 * n_row))
    if subtitle:
        fig.suptitle(subtitle, fontsize=14, fontweight='bold')
    pl.subplots_adjust(bottom=0, left=.01, right=.99, top=.90, hspace=.35)
    for i in range(n_row * n_col):
        if i >= len(images):
            break
        
        pl.subplot(n_row, n_col, i + 1)
        pl.imshow(images[i].reshape((h, w)), cmap=pl.gray())
        pl.title(titles[i], size=12)
        pl.xticks(())
        pl.yticks(())
        
    pl.show()
        
def plot_color_gallery(images, titles, n_row=3, n_col=4):
    """Helper function to plot a gallery of portraits"""
    pl.figure(figsize=(1.8 * n_col, 2.4 * n_row))
    pl.subplots_adjust(bottom=0, left=.01, right=.99, top=.90, hspace=.35)
    for i in range(n_row * n_col):
        if i >= len(images):
            break
        
        pl.subplot(n_row, n_col, i + 1)
        pl.imshow(images[i])
        pl.title(titles[i], size=12)
        pl.xticks(())
        pl.yticks(())
        
    pl.show()