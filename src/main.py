#coding: utf8

from collections import OrderedDict
from featureextraction.DescriptionsProvider import DescriptionsProvider
from featureextraction.VocabularyProvider import VocabularyProvider
from filter.SVM import SVM
from imagesearch.GoogleImageProvider import GoogleImageProvider
from matplotlib import cm as colormap
from preprocessing.Preprocessor import Preprocessor
import helper.resultDirs as dirHelper
import itertools
import json
import numpy
import os
import pylab
from featureextraction.VocabularyOptimizer import VocabularyOptimizer
import shutil

def generateDescriptions( imgSet, baseDir, imgCount, preproc, siftConfig, preprocCache = None, siftCache = None ) :
  images = GoogleImageProvider( imgSet, baseDir, imgCount )
  preprocImgs = Preprocessor( images, preproc, preprocCache )
  return DescriptionsProvider( preprocImgs, siftConfig, siftCache )

#---------------------------------------------------------------------------------------------------
# load imgFilter configuration
jsonDecoder = json.JSONDecoder( object_pairs_hook = OrderedDict )
configJSON = open( "config.json" ).read()
config = jsonDecoder.decode( configJSON )

#---------------------------------------------------------------------------------------------------
# prepare result directories
resultsDirPath, paths = dirHelper.createDirPaths( config, "../result/" )
if not os.path.isdir( resultsDirPath ) :
  os.makedirs( resultsDirPath )

#---------------------------------------------------------------------------------------------------
# cache-settings
imageLocation = "../img/custom/"
preprocessingCachePath = paths[ "preprocessing" ] + "cache/"
descrCachePath =  paths[ "sift" ] + "cache/"
vocabularyCachePath = paths[ "vocabulary" ] + "cache/vocabulary.pkl"
vocabularyName = "vocabulary.pkl"
goodImagePath = resultsDirPath + "train-images/"
badImagePath = resultsDirPath + "rejected-images/"
siftPlotPath = resultsDirPath + "sift-plots/"

unfilteredImagePath = imageLocation + config["imageSet"]["searchTerm"] + "/"
unfilteredImageSet = generateDescriptions(
  imgSet = "train",
  baseDir = unfilteredImagePath,
  imgCount = config[ "vocabulary" ][ "imageCount" ],
  preproc = config[ "preprocessing" ],
  siftConfig = config[ "sift" ],
  preprocCache = preprocessingCachePath,
  siftCache = descrCachePath
)

#---------------------------------------------------------------------------------------------------
# find best vocabulary params (number of words and seed)
if config[ "vocabulary" ][ "words" ] == "best" :
  if os.path.isdir( "tempcache" ) :
    shutil.rmtree( "tempcache" )
  noiseImagePath = "../img/custom/__noise__/"
  noisePreprocCachePath = "tempcache/noise/preproc/"
  noiseSiftCachePath = "tempcache/noise/sift/"
  trainOutSet = generateDescriptions(
    imgSet = "train",
    baseDir = noiseImagePath,
    imgCount = 190,
    preproc = config[ "preprocessing" ],
    siftConfig = config[ "sift" ],
    preprocCache = noisePreprocCachePath,
    siftCache = noiseSiftCachePath
  )
  testInSet = generateDescriptions( 
    imgSet = "test",
    baseDir = unfilteredImagePath,
    imgCount = 10,
    preproc = config[ "preprocessing" ],
    siftConfig = config[ "sift" ],
    preprocCache = preprocessingCachePath,
    siftCache = descrCachePath
  )
  testOutSet = generateDescriptions(
    imgSet = "test",
    baseDir = noiseImagePath,
    imgCount = 10,
    preproc = config[ "preprocessing" ],
    siftConfig = config[ "sift" ],
    preprocCache = noisePreprocCachePath,
    siftCache = noiseSiftCachePath
  )
  visWordsRange = 700, 700
  visWordsStep = 100
  seedAttempts = 10
  optimizer = VocabularyOptimizer(
    trainSet = unfilteredImageSet,
    trainOutSet = trainOutSet,
    testInSet = testInSet,
    testOutSet = testOutSet,
    visWordsRange = visWordsRange,
    visWordsStep  = visWordsStep,
    seedAttempts  = seedAttempts
  )
  words, errorRate = optimizer.findBestNumOfWords()
  config[ "vocabulary" ][ "words" ] = words
  config[ "vocabulary" ][ "errorRate" ] = errorRate
  config[ "vocabulary" ][ "visualWordsRange" ] = visWordsRange
  config[ "vocabulary" ][ "seedAttempts" ] = seedAttempts

seed = None
if "seed" in config[ "vocabulary" ] :
  if config[ "vocabulary" ][ "seed" ] == "best" :
    seed, errorRate = optimizer.findBestSeedForNumOfWords( words )
    config[ "vocabulary" ][ "seed" ] = seed

#---------------------------------------------------------------------------------------------------
# vocabulary
vocProvider = VocabularyProvider(
  unfilteredImageSet,
  vocabularyCachePath,
  config[ "vocabulary" ][ "words" ],
  numpy.array( seed ) if seed is not None else None
)
vocabulary = vocProvider.getVocabulary()

#---------------------------------------------------------------------------------------------------
# filtering images
def getFilterAlg( name ) :
  if name == "svm" :
    return SVM
  raise NameError( "Filter " + name + " not found." )

imgFilter = getFilterAlg( config[ "filter" ][ "type" ] )( vocabulary.bagsOfWords )


#---------------------------------------------------------------------------------------------------
# save filtered imgs
goodImages = []
badImages = []
for isGoodImg, imgAndName in itertools.izip( imgFilter.classAffiliation, unfilteredImageSet._images ) :
  if isGoodImg :
    goodImages.append( imgAndName )
  else :
    badImages.append( imgAndName )
 
if not os.path.exists( goodImagePath ) :
  os.makedirs( goodImagePath )
    
if not os.path.exists( badImagePath ) :
  os.makedirs( badImagePath )
    
for image, imageName in goodImages :
  image.save( goodImagePath + imageName )
    
for image, imageName in badImages :
  image.save( badImagePath + imageName )
     
#---------------------------------------------------------------------------------------------------
# save config file
configDump = open( resultsDirPath + "config.json", "w+" )
configDump.write( json.dumps( config, indent = 2, separators=(',', ': ')) )
configDump.close()

#---------------------------------------------------------------------------------------------------
# save sift plots
if not os.path.exists( siftPlotPath ) :
  os.makedirs( siftPlotPath )
 
imagesToPlot = [ ( image, imageName ) for image, imageName in unfilteredImageSet._images ]
for description, ( image, imageName ) in itertools.izip( unfilteredImageSet, imagesToPlot ) :
  print "plotting sift features to image", imageName
  imageArray = numpy.array( image )
  featLocations = description.featureLocations
  pylab.imshow( imageArray, cmap = colormap.Greys_r )
  pylab.plot( featLocations[ : , 0 ], featLocations[ : , 1 ], "ob" )
  pylab.savefig( siftPlotPath + imageName + ".jpg" )
  pylab.clf()
    
print "done"
