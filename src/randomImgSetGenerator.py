import os
import numpy
import shutil

imagePath = "../img/custom/__caltech101__/"
destinationPath = "../img/custom/__noise__/"
imgDirs = os.listdir( imagePath )
imagesToCopy = []

for imgDir in imgDirs :
  imgPath = imagePath + imgDir + "/"
  imgs = os.listdir( imgPath )
  randomIndices = numpy.random.random_integers( low = 0, high = len( imgs ) - 1, size = 2 )
  for i in randomIndices :
    img = imgs[ i ]
    imagesToCopy.append( ( imgPath + img, destinationPath + imgDir + "-" + img ) )

if not os.path.isdir( destinationPath ) :
  os.makedirs( destinationPath )

for source, destination in imagesToCopy :
  print "copying " + source + " to " + destination
  shutil.copy( source, destination )
