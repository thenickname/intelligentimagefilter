'''
Created on 21.04.2013

@author: Nette
'''
import clusters
import similarityScores as sim

class Hcl(object):
    '''
    classdocs
    '''


    def __init__(self,name, data, imlist):
        '''
        Constructor
        '''
        self.name = name
        self.data = data
        self.hcl=object
        self.imlist = imlist
    
    def getCluster(self):
        
        self.hcl=clusters.hcluster(self.data, distance=sim.euclidean)
        clusters.drawdendrogram(self.hcl, self.imlist, self.name+"-hcl.jpeg")
        
        return