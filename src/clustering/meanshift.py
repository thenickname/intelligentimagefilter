'''
Created on 21.04.2013

@author: Nette
'''
import numpy as np
from sklearn.cluster import MeanShift, estimate_bandwidth

class Meanshift(object):
    '''
    classdocs
    '''


    def __init__(self, data):
        '''
        Constructor
        '''
        self.data = data
        self.bandwidth = estimate_bandwidth(data, quantile=0.2)
        self.cl=object
    
    def getCluster(self):
        self.cl = MeanShift(bandwidth=self.bandwidth, bin_seeding=True)
        self.cl.fit(self.data)
        labels = self.cl.labels_
        cluster_centers = self.cl.cluster_centers_
        
        labels_unique = np.unique(labels)
        n_clusters_ = len(labels_unique)
        
        print "number of estimated clusters : %d" % n_clusters_
        print labels_unique
        
        return labels
        