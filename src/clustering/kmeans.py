'''
Created on 21.04.2013

@author: Nette
'''

import numpy as np
from scipy.cluster import vq

class Kmeans(object):
    '''
    classdocs
    '''


    def __init__(self, data):
        '''
        Constructor
        '''
        self.data = data
    
    def getCluster(self,k):
        n = self.data.shape[0]/k
        seeds = np.array(self.data[0])
        for i in range(1,k):
            seeds = np.vstack((seeds,self.data[i*n]))
        cms,distortion = vq.kmeans(self.data,seeds,20)
        cluster,_ = vq.vq(self.data,cms)
        
        return cluster
        