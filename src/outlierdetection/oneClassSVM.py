'''
Created on 21.04.2013

@author: Nette
'''

from sklearn import svm
import numpy as np
from scipy import stats

class OneClassSVM(object):
    '''
    classdocs
    '''


    def __init__(self, data):
        '''
        Constructor
        '''
        self.outliers_fraction = 0.25
        self.clusters_separation = [0, 1, 2]
        self.classifiers = {"One-Class SVM": svm.OneClassSVM(nu=0.95 * self.outliers_fraction + 0.05,kernel="rbf", gamma=0.1)}
        self.data = data
    
    def getOutliers(self):
        # Fit the problem with varying cluster separation
        for i, offset in enumerate(self.clusters_separation):
            np.random.seed(42)
            # Data 
            # Fit the model with the One-Class SVM
            for i, (clf_name, clf) in enumerate(self.classifiers.iteritems()):
                # fit the data and tag outliers
                clf.fit(self.data)
                y_pred = clf.decision_function(self.data).ravel()
                threshold = stats.scoreatpercentile(y_pred,
                                                    100 * self.outliers_fraction)
                y_pred = y_pred > threshold
                
        #returns only the y_pred of the last iteration
        return y_pred
            