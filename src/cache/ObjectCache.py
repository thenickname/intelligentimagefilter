'''
Created on 08.05.2013

@author: nick
'''
import os
import cPickle as pickle
import string

class ObjectCache( object ) :
  
  @staticmethod
  def load( path ) :
    if not os.path.isfile( path ) :
      raise Exception( "Cached object not found" )
    with open( path ) as cacheFile :
      result = pickle.load( cacheFile )
    return result
  
  @staticmethod
  def save( obj, path ) :
    pathElements = path.split( "/" )
    dirPath = string.join( pathElements[:-1], "/" ) + "/"
    filename = pathElements[-1]
    if not os.path.exists( dirPath ) :
      os.makedirs( dirPath )
    with open( dirPath + filename, "w+" ) as cacheFile :
      pickle.dump( obj, cacheFile )