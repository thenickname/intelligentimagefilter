from cache.ObjectCache import ObjectCache
from featureextraction.ImageDescription import ImageDescription
import os
import shutil

class DescriptionsProvider( object ) :

  def __init__( self, images, siftConfig, cachePath = None ) :
    self._images = images
    self._cachePath = cachePath
    self._siftConfig = siftConfig
    self.numOfDescr = images._numOfImgs

  def __iter__( self ) :
    self._images.__iter__()
    return self

  def next( self ) :
    try :
      image, imageName = self._images.next()
      if self._cachePath is not None :
        siftCacheFileName = self._cachePath + imageName + ".pkl"
        siftOrigCacheFileName = self._cachePath + imageName + ".sift"
        if os.path.isfile( siftCacheFileName ) :
          result = ObjectCache.load( siftCacheFileName )
        else :
          result = self._createImageDescription( image )
          ObjectCache.save( result, siftCacheFileName )
          shutil.copyfile( result.siftFilePath, siftOrigCacheFileName )
      else :
        result = self._createImageDescription( image )
      return result
    except StopIteration :
      raise StopIteration()

  def _createImageDescription( self, image ) :
    if self._siftConfig[ "sampling" ] == "sparse" :
      return self._createSparseDescription( image )
    elif self._siftConfig[ "sampling" ] == "dense" :
      return self._createDenseDescription( image )
    else :
      raise ValueError( "sampling value must be sparse or dense" )

  def _createSparseDescription( self, image ) :
    return ImageDescription(
      image,
      sparseEdgeTresh = self._siftConfig[ "edge" ],
      sparsePeakTresh = self._siftConfig[ "peak" ]
    )

  def _createDenseDescription( self, image ) :
    return ImageDescription(
      image,
      dense = True,
      denseFrameSize = self._siftConfig[ "frameSize" ],
      denseFrameStep = self._siftConfig[ "frameStep" ]
    )
