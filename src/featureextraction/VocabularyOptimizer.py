from featureextraction.Vocabulary import Vocabulary
from sklearn import svm
import collections
import itertools
import numpy
from filter.SVM import SVM

class VocabularyOptimizer( object ) :

  def __init__( self,
                trainSet,
                trainOutSet,
                testInSet,
                testOutSet,
                visWordsRange = ( 100, 1000 ),
                visWordsStep = 100,
                seedAttempts = 10 ) :
    if seedAttempts < 1 :
      raise ValueError( "seedAttempts must be an integer of at least 1, but was: "
                      + str( seedAttempts ) )
    if visWordsStep > visWordsRange[ 1 ] :
      raise ValueError( "visWordsStep mustn't be greater than visWordsRange" )
    if visWordsRange[ 0 ] % visWordsStep != 0 or visWordsRange[ 1 ] % visWordsStep != 0 :
      raise ValueError( "visWordsRange values must be divisible by visWordsStep" )
    self._evaluationResults = self._evaluateVocabularyPerformance( trainSet,
                                                                   trainOutSet,
                                                                   testInSet,
                                                                   testOutSet,
                                                                   visWordsRange,
                                                                   visWordsStep,
                                                                   seedAttempts )

  @property
  def evaluationResults( self ) :
    return self._evaluationResults
  
  def findBestNumOfWords( self ) :
    errorRate = None
    numOfWords = None
    for key, value in self._evaluationResults.iteritems() :
      if not errorRate or errorRate > value[ "avgError" ] :
        errorRate = value[ "avgError" ]
        numOfWords = int( key )
    return numOfWords, errorRate

  def findBestSeedForNumOfWords( self, numOfWords ) :
    errorRate = None
    seed = None
    for key, value in self._evaluationResults[ str( numOfWords ) ][ "seedAttempts" ].iteritems() :
      if not errorRate or errorRate > value[ "overallError" ] :
        errorRate = value[ "overallError" ]
        seed = value[ "seed" ]
    return seed, errorRate

  def _evaluateVocabularyPerformance( self,
                                      trainSet,
                                      trainOutSet,
                                      testInSet,
                                      testOutSet,
                                      visWordsRange,
                                      visWordsStep,
                                      seedAttempts ) :
    result = collections.OrderedDict()
    rangeFrom = visWordsRange[0]
    rangeTo = visWordsRange[1] + visWordsStep
    stackedTrainSet = self._stackFeatures( trainSet )
    stackedTrainOutSet = self._stackFeatures( trainOutSet )
    stackedTestInSet = self._stackFeatures( testInSet )
    stackedTestOutSet = self._stackFeatures( testOutSet )
    for numberOfWords in xrange( rangeFrom, rangeTo, visWordsStep ) :
      print "Optimizer: K=" + str( numberOfWords )
      numOfWordsKey = str( numberOfWords )
      result[ numOfWordsKey ] = collections.OrderedDict()
      result[ numOfWordsKey ][ "seedAttempts" ] = collections.OrderedDict()
      errorRates = []
      for i in xrange( seedAttempts ) :
        print "Optimizer: seed attempt", i
        trainVoc = Vocabulary( trainSet, numberOfWords, stackedFeatures = stackedTrainSet )
        trainOutVoc = Vocabulary( trainOutSet, vocabulary = trainVoc, stackedFeatures = stackedTrainOutSet )
        inliers, outliers = self._separateBoWs( trainVoc.bagsOfWords )
        if inliers.size > 0 :
          combinedVocs = numpy.vstack( ( inlier, outlier ) for inlier, outlier in itertools.izip( inliers, trainOutVoc.bagsOfWords ) )
          classifier = self._getTrainedClassifier( inliers, trainOutVoc, combinedVocs )
          testInVoc = Vocabulary( testInSet, vocabulary = trainVoc, stackedFeatures = stackedTestInSet )
          testOutVoc = Vocabulary( testOutSet, vocabulary = trainVoc, stackedFeatures = stackedTestOutSet )
          inClassErrorRate = self._calcPredictionErrorRate( classifier, testInVoc, label = 1 )
          outClassErrorRate = self._calcPredictionErrorRate( classifier, testOutVoc, label = 0 )
          avgErrorRate = ( inClassErrorRate + outClassErrorRate ) / 2
        else :
          inClassErrorRate = 1
          outClassErrorRate = 1
          avgErrorRate = 1
        seedKey = "seed" + str( i + 1 )
        result[ numOfWordsKey ][ "seedAttempts" ][ seedKey ] = collections.OrderedDict()
        result[ numOfWordsKey ][ "seedAttempts" ][ seedKey ][ "seed" ] = trainVoc.seed.tolist()
        result[ numOfWordsKey ][ "seedAttempts" ][ seedKey ][ "inClassErrorRate" ] = inClassErrorRate
        result[ numOfWordsKey ][ "seedAttempts" ][ seedKey ][ "outClassErrorRate" ] = outClassErrorRate
        result[ numOfWordsKey ][ "seedAttempts" ][ seedKey ][ "overallError" ] = ( inClassErrorRate + outClassErrorRate ) / 2
        errorRates.append( avgErrorRate )
      result[ numOfWordsKey ][ "avgError" ] = sum( errorRates ) / len( errorRates )
    return result

  def _stackFeatures( self, features ) :
    return numpy.vstack( ( descriptor.features for descriptor in features ) )

  def _separateBoWs( self, bows ) :
    imageFilter = SVM( bows )
    inliers = self._getClassMembers( bows, imageFilter.classAffiliation )
    outliers = self._getClassMembers( bows, imageFilter.classAffiliation, inliers = False )
    return inliers, outliers

  def _getClassMembers( self, bows, classMembership, inliers = True ):
    indices = [ i for i, inlier in enumerate( classMembership ) if inlier == inliers ]
    return bows[ numpy.array( indices ) ] if indices else numpy.array( [] )

  def _getTrainedClassifier(self, trainInVoc, trainOutVoc, combinedVocs ) :
    classifier = svm.SVC()
    classLabels = numpy.hstack((inLabel, outLabel) for (inLabel, outLabel) in itertools.izip(numpy.ones(trainInVoc.shape[0]), numpy.zeros(trainOutVoc.bagsOfWords.shape[0])))
    classifier.fit( combinedVocs, classLabels )
    return classifier

  def _calcPredictionErrorRate( self, classifier, voc, label ) :
    predictions = classifier.predict( voc.bagsOfWords )
    predictionErrors = 0.0
    for prediction in predictions :
      if prediction != label :
        predictionErrors += 1
    return predictionErrors / len( predictions )

