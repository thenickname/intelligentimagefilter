from cache.ObjectCache import ObjectCache
from featureextraction.Vocabulary import Vocabulary

class VocabularyProvider( object ) :
  
  def __init__( self, imageDescriptions, vocabularyCachePath, words, seed ) :
    self._vocabularyCachePath = vocabularyCachePath
    self._descriptions = imageDescriptions
    self._words = words
    self._seed = seed
  
  def getVocabulary( self ) :
    result = None
    try :
      print "loading vocabulary from cache"
      result = ObjectCache.load( self._vocabularyCachePath )
    except :
      print "creating vocabulary"
      result = Vocabulary( imageDescriptions = self._descriptions,
                           numberOfWords = self._words,
                           seed = self._seed )
      ObjectCache.save( result, self._vocabularyCachePath )
    return result