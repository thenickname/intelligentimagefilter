import numpy
import inspect

class RandomStateAdapter( numpy.random.RandomState ) :

  def __init__( self, seed = None ) :
    super( RandomStateAdapter, self ).__init__( seed )
    doNotOverride = [ "randint", "random_sample" ]
    def notImplementedStub( *args ) :
      raise NotImplementedError( "this method was deliberately replaced to indicate calls" )
    members = inspect.getmembers( numpy.random.RandomState( seed ), predicate = inspect.isbuiltin )
    for memberKey, memberValue in members :
      if not memberKey.startswith( "_" ) and memberKey not in doNotOverride :
        setattr( self, memberKey, notImplementedStub )

  def randint( self, low, high = None, size = None ) :
    result = super( RandomStateAdapter, self ).randint( low, high, size )
    print "------------------------------------------------------------"
    print "radint", result
    print "------------------------------------------------------------"
    return result

  def random_sample( self, size = None ) :
    result = super( RandomStateAdapter, self ).random_sample( size )
    print "------------------------------------------------------------"
    print "random_sample", result
    print "------------------------------------------------------------"
    return result