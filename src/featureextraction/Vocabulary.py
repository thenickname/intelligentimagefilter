from scipy.cluster.vq import vq, whiten
from sklearn.cluster.k_means_ import MiniBatchKMeans
import numpy

class Vocabulary( object ) :

  def __init__( self, imageDescriptions, numberOfWords = None, vocabulary = None, seed = None, stackedFeatures = None ) :
    self._imageDescriptions = imageDescriptions
    if stackedFeatures is None :
      stackedFeatures = numpy.vstack( ( descriptor.features for descriptor in self._imageDescriptions ) )
    self._whitenedStackedFeats = whiten( stackedFeatures )
    if vocabulary is None :
      self._initWithoutVoc( numberOfWords, seed )
    else :
      self._initWithVoc( vocabulary, numberOfWords, seed )
    self._bagsOfWords = self._generateBagsOfWords()

  def _initWithoutVoc( self, numberOfWords, seed ) :
    if numberOfWords is None:
      raise ValueError( "You must either provide the numberOfWords or a vocabulary" )
    self._numberOfWords = numberOfWords
    if seed is None:
      randomGenerator = numpy.random.RandomState()
      seed = randomGenerator.random_integers( 0, self._numberOfWords - 1, self._numberOfWords )
    self._seed = seed
    self._visualWords = self._generateVisualWords()
    
  def _initWithVoc( self, vocabulary, numberOfWords, seed ) :
    if numberOfWords is not None:
      raise ValueError( "Providing vocabulary and numberOfWords at the same time is not permitted." )
    if seed is not None:
      raise ValueError( "Providing vocabulary and seed at the same time is not permitted." )
    self._numberOfWords = vocabulary.numberOfWords
    self._seed = vocabulary.seed
    self._visualWords = vocabulary.visualWords
    
  @property
  def numberOfWords( self ) :
    return self._numberOfWords
  
  @property
  def seed( self ) :
    return self._seed
  
  @property
  def visualWords( self ) :
    return self._visualWords

  @property
  def bagsOfWords( self ) :
    return self._bagsOfWords

  def _generateVisualWords( self ) :
    slices = self._slice( self._whitenedStackedFeats )
    batchKMeans = MiniBatchKMeans( k = self._numberOfWords,
                                   random_state = numpy.random.RandomState( self.seed ) )
    for descSlice in slices :
      batchKMeans.partial_fit( descSlice )
    return batchKMeans.cluster_centers_
  
  def _slice( self, features ) :
    sliceSize = int( features.shape[ 0 ] / self._numberOfWords )
    result = numpy.array_split( features, sliceSize )
    return result

  def _generateBagsOfWords( self ) :
    result = numpy.zeros( ( self._imageDescriptions._images._numOfImgs, self._numberOfWords ) )
    featIndex = 0
    for i, descr in enumerate( self._imageDescriptions ) :
      descrShape = descr.features.shape
      features = self._whitenedStackedFeats[ featIndex : featIndex + descrShape[ 0 ] , : ]
      result[ i ] = self._generateBagOfWords( features )
      featIndex += descrShape[ 0 ]
    return result

  def _generateBagOfWords( self, descriptors ) :
    result = numpy.zeros( ( self._numberOfWords ) )
    matchedWordsIndices, distances = vq( descriptors, self.visualWords )
    for wordIndex in matchedWordsIndices:
      result[ wordIndex ] += 1
    return result
