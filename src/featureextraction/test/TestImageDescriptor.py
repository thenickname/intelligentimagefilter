'''
Created on 01.05.2013

@author: nick
'''
import unittest
from featureextraction.ImageDescriptor import ImageDescriptor
import Image
import os
import numpy


class TestImageDescriptor( unittest.TestCase ) :

    def setUp( self ) :
      self.originalImageName = "originalTestImage.jpg"
      self.preparedImageName = "preparedImage.pgm"
      self.siftOutputFileName = "expectedOutput.sift"
      
    def tearDown( self ) :
      self._removeFile( self.preparedImageName )
      self._removeFile( self.siftOutputFileName )
      self.originalImageName = None
      self.preparedImageName = None
      self.siftOutputFileName = None
      
    def testFeaturesAndFeatureLocations( self ) :
      expectedFeatures, expectedFeatureLocations = self._createExpectedDescriptorData()
      imgDescr = ImageDescriptor( Image.open( self.originalImageName ) )
      self.assertTrue( numpy.array_equal( expectedFeatures, imgDescr.features ) )
      self.assertTrue( numpy.array_equal( expectedFeatureLocations, imgDescr.featureLocations ) )
    
    def testFeaturesAndFeatureLocations_withParams( self ) :
      pass
    
    def testTemporaryFilesWereCleanedUp( self ) :
      pass
    
    def _createExpectedDescriptorData( self, siftParams = "" ):
      img = Image.open( self.originalImageName ).convert( "L" )
      img.save( self.preparedImageName )
      command = str( "sift " + self.preparedImageName + " --output=" + self.siftOutputFileName + " " + siftParams )
      result = os.system( command )
      print result
      siftResult = numpy.loadtxt( self.siftOutputFileName )
      return siftResult[:,4:], siftResult[:,:4]
    
    def _removeFile( self, fileName ) :
        try :
          os.remove( fileName )
        except :
          pass