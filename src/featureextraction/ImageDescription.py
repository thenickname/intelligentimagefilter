import os
import numpy

class ImageDescription( object ) :

  _TEMP_IMG_NAME = "temp-image.pgm"
  _TEMP_SIFT_FILE_NAME = "temp-sift.sift"
  _TEMP_DENSE_FRAMES_FILE_NAME = "temp-sift-frame.frame"

  def __init__( self,
                img,
                sparseEdgeTresh = 10,
                sparsePeakTresh = 5,
                dense = False,
                denseFrameSize = 7,
                denseFrameStep = 10,
                denseForceOrientation = True ) :
    self._prepareImgForCommand( img )
    cmmd = ""
    if dense :
      self._createFrameFile( img, denseFrameSize, denseFrameStep )
      cmmd = self._generateDenseCommand( denseForceOrientation )
    else :
      cmmd = self._generateSparseCommand( sparseEdgeTresh, sparsePeakTresh )
    systemMessage = os.system( cmmd )
    if systemMessage is not 0 :
      raise Exception( systemMessage )
    self._features, self._featureLocations = self._readSiftResults()
    self._siftFilePath = os.path.abspath( self._TEMP_SIFT_FILE_NAME )

  def __del__( self ) :
    if os.path.isfile( self._TEMP_IMG_NAME ) :
      os.remove( self._TEMP_IMG_NAME )
    if os.path.isfile( self._TEMP_SIFT_FILE_NAME ) :
      os.remove( self._TEMP_SIFT_FILE_NAME )
    if os.path.isfile( self._TEMP_DENSE_FRAMES_FILE_NAME ) :
      os.remove( self._TEMP_DENSE_FRAMES_FILE_NAME )

  @property
  def features( self ) :
    return self._features

  @property
  def featureLocations( self ) :
    return self._featureLocations

  @property
  def siftFilePath( self ) :
    return self._siftFilePath

  def _prepareImgForCommand( self, img ) :
    img.save( self._TEMP_IMG_NAME )

  def _createFrameFile( self, img, frameSize, frameStep ) :
    imgSizeX, imgSizeY = img.size
    scale = frameSize / 1.0
    x, y = numpy.meshgrid( 
      range( frameStep, imgSizeX, frameStep ),
      range( frameStep, imgSizeY, frameStep )
    )
    xx, yy = x.flatten(), y.flatten()
    frame = numpy.array( [ xx, yy, scale * numpy.ones( xx.shape[ 0 ] ), numpy.zeros( xx.shape[ 0 ] ) ] )
    numpy.savetxt( self._TEMP_DENSE_FRAMES_FILE_NAME, frame.T, fmt="%03.3f" )

  def _generateSparseCommand( self, edgeTresh, peakTresh ) :
    return str( "sift "
              + self._TEMP_IMG_NAME + " "
              + "--output=" + self._TEMP_SIFT_FILE_NAME + " "
              + "--edge-thresh " + str( edgeTresh ) + " --peak-thresh " + str( peakTresh ) )

  def _generateDenseCommand( self, forceOrientation ) :
    orientation = "--orientations" if forceOrientation else ""
    return str( "sift "
              + self._TEMP_IMG_NAME + " "
              + "--output=" + self._TEMP_SIFT_FILE_NAME
              + " --read-frames=" + self._TEMP_DENSE_FRAMES_FILE_NAME + " "
              + orientation )
    

  def _readSiftResults( self ) :
    tempSiftFile = numpy.loadtxt( self._TEMP_SIFT_FILE_NAME )
    return tempSiftFile[:,4:], tempSiftFile[:,:4]