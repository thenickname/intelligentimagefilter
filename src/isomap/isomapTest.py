from pylab import scatter,text,show,cm,figure
from pylab import subplot,imshow,NullLocator
from sklearn import manifold, datasets
 
# load the digits dataset
# about 180 samples for the digits (0,1,2,3,4)
digits = datasets.load_digits(n_class=5)
X = digits.data
color = digits.target
 
# shows some digits
figure(1)
for i in range(36):
  ax = subplot(6,6,i)
  ax.xaxis.set_major_locator(NullLocator()) # remove ticks
  ax.yaxis.set_major_locator(NullLocator())
  imshow( digits.images[ i ], cm.gray_r )
  
# running Isomap
# 5 neighbours will be considered and reduction on a 2d space
Y = manifold.Isomap(5, 2).fit_transform(X)
 
# plotting the result
figure(2)
scatter(Y[:,0], Y[:,1], c='k', alpha=0.3, s=10)
for i in range(Y.shape[0]):
  text(Y[i, 0], Y[i, 1], str(color[i]), color=cm.Dark2(color[i] / 5.), fontdict={'weight': 'bold', 'size': 11})
show()