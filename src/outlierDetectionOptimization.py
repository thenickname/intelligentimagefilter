from featureextraction.DescriptionsProvider import DescriptionsProvider
from featureextraction.Vocabulary import Vocabulary
from imagesearch.GoogleImageProvider import GoogleImageProvider
from preprocessing.Preprocessor import Preprocessor
from matplotlib import cm as colormap
from scipy import stats
from sklearn import svm
import itertools
import numpy
import os
import pylab


cacheLocation = "tempcache/"
siftPlotPath = cacheLocation + "sift-plots/"

images = GoogleImageProvider( "bananas-hand-picked-and-polluted", "../img/custom/", numOfImgs = 119 )
preprocessedImgs = Preprocessor( images, { "resize":800, "colorSpace":"L" }, cacheLocation + "preproc/" )
descriptions = DescriptionsProvider( preprocessedImgs, 30, 3, cacheLocation + "sift/" )
voc = Vocabulary( descriptions, 700 )

outlierDetector = svm.OneClassSVM( nu = 0.05 )
outlierDetector.fit( voc.bagsOfWords )
distancesToHyperplane = outlierDetector.decision_function( voc.bagsOfWords ).ravel()
print distancesToHyperplane

threshold = stats.scoreatpercentile( distancesToHyperplane, 25 )
result = distancesToHyperplane > threshold
for ( image, imageName ), isInlier in itertools.izip( images, result ) :
  print imageName, isInlier

# #---------------------------------------------------------------------------------------------------
# # save sift plots
# if not os.path.exists( siftPlotPath ) :
#   os.makedirs( siftPlotPath )
#  
# imagesToPlot = [ ( image, imageName ) for image, imageName in preprocessedImgs ]
# for description, ( image, imageName ) in itertools.izip( descriptions, imagesToPlot ) :
#   print "plotting sift features to image", imageName
#   imageArray = numpy.array( image )
#   featLocations = description.featureLocations
#   pylab.imshow( imageArray, cmap = colormap.Greys_r )
#   pylab.plot( featLocations[ : , 0 ], featLocations[ : , 1 ], "ob" )
#   pylab.savefig( siftPlotPath + imageName + ".jpg" )
#   pylab.clf()