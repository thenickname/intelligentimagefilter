'''
Created on 24.04.2013

@author: nick
'''

import abc
import os

class ImageProvider( object ):

  __metaclass__ = abc.ABCMeta
  
  _imageList = None
  
  @abc.abstractmethod
  def __iter__( self ) :
    pass
  
  @abc.abstractmethod
  def next( self ) :
    pass
