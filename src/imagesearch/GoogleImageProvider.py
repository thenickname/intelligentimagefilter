'''
Created on 24.04.2013

@author: nick
'''

from imagesearch.ImageProvider import ImageProvider
from imagesearch.BirdAPI import google as google
import os
import urllib
import Image

class GoogleImageProvider( ImageProvider ) :
  
  CONTENT_TYPES = ( "image/jpeg", "image/jpg" ) #, "image/png", "image/gif" )
  FILE_EXTENSIONS = ( "jpeg", "jpg", "JPG" ) #, "png", "gif" )
  _imageSet = None
  _numOfImgs = None
  _imageLocation = None
  _index = 0
  _length = 0
  
  def __init__( self, imageSet, basePath, numOfImgs, downloadImgsHint = 100 ) :
    if downloadImgsHint % 20 != 0 :
      raise Exception( "Amount of images to download must be devisible by 20, but was " + downloadImgsHint )
    self._pages = downloadImgsHint / 20
    self._imageSet = imageSet
    self._numOfImgs = numOfImgs
    self._imageLocation = basePath + imageSet.replace( " ", "_" ) + "/"
    self._imageList = self._getImageList()
    self._length = len( self._imageList )
    
  def _getImageList( self ) :
    if not os.path.isdir( self._imageLocation ) :
      os.makedirs( self._imageLocation )
    result = self._getLocalImages()
    if not result :
      self._downloadImages()
      result = self._getLocalImages()
    numOfLocalImgs = len( result )
    if numOfLocalImgs < self._numOfImgs :
      raise Exception( "Not enough images. Requested "
                     + str( self._numOfImgs )
                     + " image(s), but found only "
                     + str( numOfLocalImgs )
                     + " image(s)." )
    return result

# TODO exception wenn zu wenige Bilder im Ordner
  def _getLocalImages( self ) :
    result = []
    for fileName in os.listdir( self._imageLocation ) :
      if os.path.isfile( self._imageLocation + fileName ) :
        for extension in self.FILE_EXTENSIONS :
          if fileName.endswith( "." + extension ) :
            result.append( fileName )
            break
    result.sort()
    if len( result ) == 0 :
      return result
    return result[:self._numOfImgs]

  def _downloadImages( self ) :
    print "downloading ~" + str( 20 * self._pages ) + " images."
    googleResult = google.Google.search_images( query = self._imageSet, pages = self._pages )
    i = 0
    for item in googleResult :
      try :
        filename = self._imageLocation + self._imageSet.replace( " ", "_" ) + str( i ).zfill( 3 ) + "." + item.format.split( "%" )[0]
        contentType = self._getContentType( item )
        if contentType in self.CONTENT_TYPES :
          urllib.urlretrieve( item.link , filename )
          i += 1
      except :
        print 'Download Error'
    
  def _getContentType( self, item ) :
    resource = urllib.urlopen( item.link )
    result = resource.info()[ "Content-Type" ]
    resource.close()
    return result

  def __iter__( self ) :
    self._index = 0
    return self
  
  def next( self ) :
    if self._index < self._length :
      try :
        resultImage = Image.open( self._imageLocation + self._imageList[ self._index ] )
        resultImageName = self._imageList[ self._index ]
        self._index += 1
        return resultImage, resultImageName
      except Exception, e :
        print e, "error loading image " + self._imageList[ self._index ]
    else :
      raise StopIteration
