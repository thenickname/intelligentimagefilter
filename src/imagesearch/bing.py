'''
Created on 26.03.2013

@author: Nette
'''
import urllib2
import json
import urllib

class Bing(object):
    '''
    classdocs
    '''


    def __init__(self,username='nette-land@web.de',accountKey='JJh3VXhBLCGdLcNABfDxUcB1ipeOMB8NBKa84j3db2g='):
        '''
        Constructor
        '''
        self.username = username
        self.accountKey = accountKey
    
    def imageUrls(self,searchterm, size='Medium',style='Photo', limit=20):
        imgUrls = []
        baseUrl = "https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Image"
        searchUrl = "?Query=%27"+urllib.quote(searchterm)+"%27"
        #optionsUrl = "&Market=%27de-DE%27&ImageFilters=%27Size%3a"+size+"%2bStyle%3a"+style+"%27&$top="+str(limit)+"&$format=json"
        optionsUrl = "&Market=%27de-DE%27&$top="+str(limit)+"&$format=json"
        searchURL = baseUrl+searchUrl+optionsUrl
        
        password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
        password_mgr.add_password(None, searchURL,self.username,self.accountKey)
        
        handler = urllib2.HTTPBasicAuthHandler(password_mgr)
        opener = urllib2.build_opener(handler)
        urllib2.install_opener(opener)
        r = urllib2.urlopen(searchURL).read()
        r = json.loads(r)
        results = r['d']['results']
        for item in results:
            imgUrls.append(item['MediaUrl'])
            
        return imgUrls

        