import abc

class DiscriminationStrategy( object ) :
  
  __metaclass__ = abc.ABCMeta
  
  @abc.abstractproperty
  def classAffiliation( self ) :
    pass