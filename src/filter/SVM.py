from filter.DiscriminationStrategy import DiscriminationStrategy
from scipy import stats
from sklearn import svm

class SVM( DiscriminationStrategy ) :

  _OUTLIERS_FRACTION = 0.25
  _GAMMA = 0.1
  _KERNEL = "rbf"

  def __init__( self, bagsOfWords ) :
    svmClassifier = svm.OneClassSVM( kernel = SVM._KERNEL, gamma = SVM._GAMMA )
    svmClassifier.fit( bagsOfWords )
    classMembership = svmClassifier.decision_function( bagsOfWords ).ravel()
    threshold = stats.scoreatpercentile( classMembership, 100 * SVM._OUTLIERS_FRACTION )
    classMembership = classMembership > threshold
    self._classMembership = classMembership

  @property
  def classAffiliation( self ) :
    return self._classMembership
