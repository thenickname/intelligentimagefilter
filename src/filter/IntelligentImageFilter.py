import itertools

class IntelligentImageFilter( object ) :

  _goodImages = None
  _badImages = None

  def __init__( self, images, bagsOfWords, filterAlgr ) :
    self._goodImages = []
    self._badImages = []
    self._goodBagsOfWords = []
    self._badBagsOfWords = []
    filteringResults = filterAlgr.applyFilter( bagsOfWords )
    for i, ( isGoodImage, image ) in enumerate( itertools.izip( filteringResults, images ) ) :
      if isGoodImage :
        self._goodImages.append( ( image ) )
        self._goodBagsOfWords.append( bagsOfWords[ i ] )
      else :
        self._badImages.append( image )
        self._badBagsOfWords.append( bagsOfWords[ i ] )

  @property
  def goodImages( self ) :
    return self._goodImages
  
  @property
  def badImages( self ) :
    return self._badImages
  
  @property
  def goodBagsOfWords( self ) :
    return self._goodBagsOfWords
  
  @property
  def badBagsOfWords( self ) :
    return self._badBagsOfWords