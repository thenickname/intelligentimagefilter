import Image
import ImageOps
import os
import numpy as np
from scipy import ndimage
import cv2

class Preprocessor( object ) :
  
  def __init__( self, images, processingSteps, cachePath = None ) :
    self._images = images
    self._cachePath = cachePath
    self._processingSteps = processingSteps
    self._numOfImgs = images._numOfImgs
    if cachePath is not None and not os.path.exists( cachePath ) :
      os.makedirs( cachePath )
  
  def __iter__( self ) :
    self._images.__iter__()
    return self

  def next( self ) :
    try :
      image, imageName = self._images.next()
      result = image
      if self._cachePath is not None :
        cacheFilename = self._cachePath + imageName
        if os.path.isfile( cacheFilename ) :
          result = Image.open( cacheFilename )
        else :
          result = self._preprocessImage( image )
          result.save( cacheFilename )
      else :
        result = self._preprocessImage( image )
      return result, imageName
    except StopIteration :
      raise StopIteration()
  
  def _preprocessImage( self, image ) :
    result = image
    for key, value in self._processingSteps.iteritems() :
      result = getattr( self, "_" + key )( result, value )
    return result
  
  def _resize( self, image, size ) :
    currentSize = image.size
    if currentSize[ 0 ] > size or currentSize[ 1 ] > size :
      image.thumbnail( ( size, size ), Image.ANTIALIAS ) 
    return image
  
  def _colorSpace( self, image, targetColorSpace ) :
    return image.convert( targetColorSpace )

  def _removeBackground( self, image, dilateIterations) :
    #invert image
    image_inverted = ImageOps.invert(image)
    oimg=np.array(image_inverted)
    
    #apply gaussian filter
    img = ndimage.gaussian_filter(oimg, 2)
    
    #convert to grayscale and find best threshold value with Otsu's binariazation
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    
    fg=thresh
    
    #Dilating the thresholded image so that background region is reduced
    #remaining black region is 100% background.set it to 128
    #dilateIterations = recommended value 6
    bgt = cv2.dilate(thresh,None,dilateIterations)
    ret,bg = cv2.threshold(bgt,1,128,1)
    
    #add both fg and bg 
    marker = cv2.add(fg,bg)
    
    #convert it into 32SC1
    marker32 = np.int32(marker)
    
    # apply watershed and convert result back into uint8 image
    cv2.watershed(img,marker32)
    m = cv2.convertScaleAbs(marker32)
    
    #threshold it properly to get the mask and perform bitwise_and with the input image
    ret,thresh = cv2.threshold(m,0,210,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    
    res = cv2.bitwise_and(oimg,oimg,mask = thresh)

    resImage=Image.fromarray(res)
    resImage_inverted = ImageOps.invert(resImage)
    res_inverted=np.array(resImage_inverted)
    
    return resImage_inverted.convert('L')