'''
Created on 07.04.2013

@author: Nette
'''
import cv2
import numpy as np
from PIL import Image
import PIL.ImageOps
import rof
from matplotlib import pyplot as plt
from scipy import ndimage
import ImageEnhance

def test():
    img = cv2.imread('../images/birdAPIgoogle_Himbeere17.jpg',0)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
    close = cv2.morphologyEx(gray,cv2.MORPH_CLOSE,kernel1)
    div = np.float32(gray)/(close)
    res = np.uint8(cv2.normalize(div,div,0,255,cv2.NORM_MINMAX))
    return res

'''
Load image, convert it to grayscale, and threshold it with a suitable value. 
With Otsu's binarization, it would find the best threshold value.
'''
image = Image.open('../images/birdAPIgoogle_Himbeere17.jpg')
#image = Image.open('../images/google_brezel18.jpg')
#img = cv2.imread('sofwatershed.jpg')
#image = Image.open('../images/birdAPIgoogle_Himbeere17.jpg')
#image = Image.open('../images/bing_brezel16.jpg')
#image = Image.open('../images/birdAPIgoogle_brezel2.jpg')




image_inverted = PIL.ImageOps.invert(image)

oimg=np.array(image_inverted)
plt.imshow(oimg)
plt.show()
#img = cv2.imread('../images/birdAPIgoogle_Himbeere17.jpg')

img = ndimage.gaussian_filter(oimg, 2)

#im = Image.fromarray(img)
#contr = ImageEnhance.Contrast(im)
#img = contr.enhance(4)
#img = np.array(img)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
#ret,thresh = rof.denoise(img,img, tolerance=0.1)

plt.imshow(gray)
plt.show()

plt.imshow(thresh)
plt.show()
'''
A - Foreground region:- Already got a threshold image where the objects are white color. 
We erode them a little, so that we are sure remaining region belongs to foreground.
'''
#fg = cv2.erode(thresh,None,iterations = 1)
fg=thresh
plt.imshow(fg)
plt.show()

'''
B - Background region :- Dilating the thresholded image so that background region is reduced. 
But we are sure remaining black region is 100% background. We set it to 128.
'''
bgt = cv2.dilate(thresh,None,iterations = 6)
ret,bg = cv2.threshold(bgt,1,128,1)


plt.imshow(bg)
plt.show()

'''
C - Now we add both fg and bg :
'''

marker = cv2.add(fg,bg)
'''
Then we convert it into 32SC1 :
'''
marker32 = np.int32(marker)

'''
3 - Finally we apply watershed and convert result back into uint8 image:
'''
cv2.watershed(img,marker32)
m = cv2.convertScaleAbs(marker32)

print type(m)
plt.imshow(m)
plt.show()

'''
4 - We threshold it properly to get the mask and perform bitwise_and with the input image:
'''
ret,thresh = cv2.threshold(m,0,210,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
#thresh_inverted = PIL.ImageOps.invert(thresh)
#PIL.ImageOps.invert
plt.imshow(thresh)
plt.show()
res = cv2.bitwise_and(oimg,oimg,mask = thresh)

resImage=Image.fromarray(res)
resImage_inverted = PIL.ImageOps.invert(resImage)
res_inverted=np.array(resImage_inverted)
#res_inverted=np.array(res)
#plt.imshow(res)
#plt.show()
cv2.imshow('Image', res_inverted) #Show the image
#cv2.resizeWindow('Image', 300, 400)
cv2.waitKey(delay=0)