'''
Created on 16.04.2013

@author: Nette
'''
from matplotlib import pyplot as plt
import cv2
import watershed
#img = cv2.imread('../images/bing_Himbeere19.jpg')
#img = cv2.imread('../images/bing_banane0.jpg')
#img = cv2.imread('../images/birdAPIgoogle_Banane00.jpg')
#img = cv2.imread('../images/birdAPIgoogle_brezel19.jpg')
img = cv2.imread('../images/birdAPIgoogle_Himbeere9.jpg')
#img = cv2.imread('../images/bing_brezel16.jpg')
#img = cv2.imread('../images/bing_brezel19.jpg')
mask = watershed.getMask(img)
plt.imshow(mask)
plt.show()
cv2.imshow('Mask',mask)
cv2.waitKey(4000)